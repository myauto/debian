
# These are Advanced options!!

# '??' in commented out lins means things between is is not sure.
# '?' in commented out lins means the only word after it is not sure.

# ?? Guess:
# The "debian-installer" runs in a ?syslinux os(named isolinux in the iso file) which is in ram,
# and the os has a shell ?"bash",
# and in fact operate files between source and target filesystem to complete the installation task.
# But note:the current os is runing in ram,and can just modify file in target os's filesystem,
# so,conmand like"apt install"which depends on the target os's runing time won't work like in the target os's runing time by "preseed/late_command"
# so just run them at the target os's first boot.
# ??

# When the filesystem of the target system is mounted, it is available in /target.
# If an installation CD is used, when it is mounted it is available in /cdrom.(https://www.debian.org/releases/jessie/amd64/apbs05.html.zh-cn)

# ?? If an installation USB is used,it might be available in /hd-media.(https://www.debian.org/releases/jessie/amd64/apbs02.html.zh-cn - 如果用 USB 设备安装(将预置文件放入 U 盘的顶极目录):preseed/file=/hd-media/preseed.cfg)
# ??

# This file "preseed_run_setings.sh" will be found by "d-i preseed/run string /scripts/preseed_run_setings.sh" 
# in file "preseed.cfg" if the preseed/url is a relative path,
# and will be download and load just after the time "preseed.cfg" was found.
# This file "preseed_run_setings.sh" is being executed at the beginning for installation,
# maybe just when it is download.

cat > /tmp/firstboot.sh << EOF
#!/bin/bash

# This file /tmp/firstboot.sh will be copy to /target/home/ and set to execute on start by "preseed/late_command" in file "preseed.cfg".

# This block is to do some cleaning when the target os's runing.

sed -i "s#bash /home/firstboot.sh##g" /etc/rc.local

rm /home/firstboot.sh

# This block is to do some basic but important configs when the target os's runing.

apt update
apt install -y netselect-apt
netselect-apt -s -n
mv sources.list /etc/apt/sources.list

sed -i "s/^deb cdrom/# deb cdrom/g" /etc/apt/sources.list

EOF

cat >> /tmp/firstboot.sh << EOF
# This file /tmp/firstboot.sh will be copy to /target/home/ and set to execute on start by "preseed/late_command" in file "preseed.cfg".

# This block is to do things that you like when the target os's runing.

sed -i -e "s/PermitRootLogin without-password/PermitRootLogin yes/g" \
-e "s/#PasswordAuthentication yes/PasswordAuthentication yes/g" /etc/ssh/sshd_config

apt update
apt -y upgrade
apt install -y lrzsz

EOF

cat >> /tmp/firstboot.sh << EOF
# This file /tmp/firstboot.sh will be copy to /target/home/ and set to execute on start by "preseed/late_command" in file "preseed.cfg".

# reboot to make sure all setings active and not lost.
reboot

EOF
